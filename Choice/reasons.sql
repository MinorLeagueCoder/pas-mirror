SELECT			ps.policynumber,
				ps.riskstatecd,
				TRUNC(ps.transactiondate) AS transdate,
				ps.agentcd,
				ps.tier,
				CASE
					WHEN dr.drivingrecord > 0 THEN 'Driving Record'
					WHEN dr.yde > 0 THEN 'Years Driving Experience'
					WHEN prf.ratedpriorbi = 'N' THEN 'No Prior Insurance'
					WHEN ps.bestcreditscore <= 650 THEN 'Low Credit'
					WHEN ps.bestcreditscore = 998 THEN 'No Hit'
					WHEN ps.bestcreditscore = 999 THEN 'No Score'
					ELSE 'Other'
				END AS singlereason,
				CASE WHEN dr.drivingrecord > 0 THEN 1 ELSE 0 END AS drv,
				CASE WHEN dr.yde > 0 THEN 1 ELSE 0 END AS yde,
				CASE WHEN prf.ratedpriorbi = 'N' THEN 1 ELSE 0 END AS priorcarrier,
				CASE WHEN ps.bestcreditscore <= 650 OR ps.bestcreditscore IN(998,999) THEN 1 ELSE 0 END AS cred
FROM			policysummary ps
	JOIN
	(
				SELECT			policynumber,
								SUM(CASE WHEN majors >= 1 OR atfaults >= 2 OR minors >= 3 THEN 1 ELSE 0 END) AS drivingrecord,
								SUM(yde) AS yde
				FROM
				(
								SELECT			ps.policynumber,
												dr.id,
												SUM(CASE WHEN dr.tyde < 3 THEN 1 ELSE 0 END) AS yde,
												SUM(CASE WHEN av.activitytype IN('arv','majv','10majv','7majv') THEN 1 ELSE 0 END) AS Majors,
												SUM(CASE WHEN av.activitytype IN('afa','mpol','pafa','pipol') OR av.activitytype = 'acc' AND av.liabilitycd = 'AF' THEN 1 ELSE 0 END) AS AtFaults,
												SUM(CASE WHEN av.activitytype IN('minv','spd','mesg') THEN 1 ELSE 0 END) AS Minors
								FROM			policysummary ps
									JOIN		driver dr
										ON		ps.policydetail_id = dr.policydetail_id
									LEFT JOIN	accidentviolation av
										ON		dr.id = av.driver_id
										AND		TRUNC(ps.transactiondate) <= ADD_MONTHS(TRUNC(av.accidentviolationdt),33)
										AND		av.includetoratingind = 1
								WHERE			dr.drivertypecd = 'afr'
								GROUP BY		ps.policynumber,
												dr.id
				)
				GROUP BY		policynumber
	) dr
		ON		ps.policynumber = dr.policynumber
	JOIN
	(
				SELECT			policyratingimageid,
								MAX(value) AS ratedpriorbi
				FROM			policyratingfactor
				WHERE			code = 'priorBILimit'
				GROUP BY		policyratingimageid
	) prf
		ON		ps.policyratingimageid = prf.policyratingimageid
	JOIN
	(
				SELECT			policydetail_id,
								SUM(CASE WHEN UPPER(firstname) LIKE 'TEST%' OR UPPER(lastname) LIKE 'TEST%' THEN 1 ELSE 0 END) AS TestVal
				FROM			insuredprincipal
				GROUP BY		policydetail_id
	) tv
		ON		ps.policydetail_id = tv.policydetail_id
WHERE			TRUNC(ps.transactiondate) <= &oracleToDate
	AND			ps.lob = 'AUTO'
	AND			ps.policyformcd = 'CHOICE'
	AND			ps.txtype = 'policy'
	AND			ps.policystatuscd = 'issued'
	AND			ps.riskstatecd <> 'CA'
	AND			SUBSTR(ps.policynumber,1,2) <> 'CA'
	AND			tv.testval = 0
